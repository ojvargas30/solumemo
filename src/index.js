import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./index.css";
import "font-awesome/css/font-awesome.css";
import "bootstrap/dist/css/bootstrap.min.css";
// import $ from 'jquery';
// import Popper from 'popper.js';
// import 'bootstrap/dist/js/bootstrap.bundle.min';
// three.js - https://github.com/mrdoob/three.js

ReactDOM.render(<App />, document.getElementById("root"));
